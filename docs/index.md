# Compte rendu

# Commandes utiles

```bash
systemctl -t service
systemctl cat wpa_supplicant.service

systemctl status multi-user.target
systemd-cgls

systemctl -t help
```

# I. systemd-basics

## 1. First steps

```bash
[root@fedora31-1 ~]# ps -ef | grep systemd
root           1       0  0 09:12 ?        00:00:02 /usr/lib/systemd/systemd --switched-root --system --deserialize 28
```

## 2. Gestion du temps

| Type           | Description                                       |
|----------------|---------------------------------------------------|
| Local Time     | L'heure spécifié par la timezone                  |
| Universal Time | Echelle de temps partagé par la majortié des pays |
| RTC Time       | Horloge interne (materiel)                        |

On peut utiliser le **temps RTC** lorsque :

* pas d'**accès au réseau**
* on veux une heure plus **précise**

Changement de **fuseau horaire**

```bash
timedatectl set-timezone Europs/Paris
```

**Désactivation** de NTP

```bash
[root@fedora31-1 ~]# timedatectl set-ntp false
[root@fedora31-1 ~]# timedatectl status
               Local time: Fri 2019-11-29 10:56:28 UTC
           Universal time: Fri 2019-11-29 10:56:28 UTC
                 RTC time: Fri 2019-11-29 10:56:28
                Time zone: Etc/UTC (UTC, +0000)
System clock synchronized: yes
              NTP service: inactive
          RTC in local TZ: no
```

## 3. Gestion de noms

Différence entre les **trois types** de nom d'hôte.

```
man hostnamectl

       This tool distinguishes three different hostnames: the high-level "pretty" hostname which might
       include all kinds of special characters (e.g. "Lennart's Laptop"), the static hostname which is
       used to initialize the kernel hostname at boot (e.g. "lennarts-laptop"), and the transient
       hostname which is a fallback value received from network configuration. If a static hostname is
       set, and is valid (something other than localhost), then the transient hostname is not used.
```

On utilisera le nom donné par le paramètre `--static` pour de la **production**

## 4. Gestion du réseau (et résolution de noms)

Récupération des **baux DHCP**
```bash
[root@fedora31-1 ~]# cat /var/lib/NetworkManager/*.lease
# This is private data. Do not parse.
ADDRESS=192.168.5.60
NETMASK=255.255.255.0
ROUTER=192.168.5.1
SERVER_ADDRESS=192.168.5.1
NEXT_SERVER=192.168.5.1
BROADCAST=192.168.5.255
T1=1646
T2=2996
LIFETIME=3600
DNS=192.168.5.1
DOMAINNAME=private
HOSTNAME=fedora31-1
CLIENTID=015254006a95b5
```

On **retire** `NetworkManager`

```bash
[root@fedora31-1 ~]# systemctl status NetworkManager
● NetworkManager.service - Network Manager
   Loaded: loaded (/usr/lib/systemd/system/NetworkManager.service; enabled; vendor preset: enabled)

[root@fedora31-1 ~]# systemctl stop NetworkManager
[root@fedora31-1 ~]# systemctl disable NetworkManager
Removed /etc/systemd/system/network-online.target.wants/NetworkManager-wait-online.service.
Removed /etc/systemd/system/multi-user.target.wants/NetworkManager.service.
Removed /etc/systemd/system/dbus-org.freedesktop.nm-dispatcher.service.
```

**Démarrage** et **activation** de `systemd-networkd`

```bash
[root@fedora31-1 ~]# systemctl start systemd-networkd
[root@fedora31-1 ~]# systemctl enable systemd-networkd
Created symlink /etc/systemd/system/dbus-org.freedesktop.network1.service → /usr/lib/systemd/system/systemd-networkd.service.
Created symlink /etc/systemd/system/multi-user.target.wants/systemd-networkd.service → /usr/lib/systemd/system/systemd-networkd.service.
Created symlink /etc/systemd/system/sockets.target.wants/systemd-networkd.socket → /usr/lib/systemd/system/systemd-networkd.socket.
Created symlink /etc/systemd/system/network-online.target.wants/systemd-networkd-wait-online.service → /usr/lib/systemd/system/systemd-networkd-wait-online.service.
```

Configuration d'une **interface** gérée par le démon `systemd-networkd`
```bash
cat > /etc/systemd/network/eth0.network <<EOF
[Match]
Key=eth0

[Network]
DHCP=ipv4
EOF

systemctl restart systemd-networkd
```


**Démarrage** et **activation** de `systemd-resolved`

```bash
[root@fedora31-1 tonychg]# systemctl start systemd-resolved
[root@fedora31-1 tonychg]# systemctl enable systemd-resolved
Created symlink /etc/systemd/system/dbus-org.freedesktop.resolve1.service → /usr/lib/systemd/system/systemd-resolved.service.
Created symlink /etc/systemd/system/multi-user.target.wants/systemd-resolved.service → /usr/lib/systemd/system/systemd-resolved.service.
```

Quels sonts les ports **ouverts** ?

```bash
[root@fedora31-1 tonychg]# ss -utapnl | grep resolve
udp    UNCONN  0       0                    0.0.0.0:5355          0.0.0.0:*      users:(("systemd-resolve",pid=1446,fd=12))
udp    UNCONN  0       0              127.0.0.53%lo:53            0.0.0.0:*      users:(("systemd-resolve",pid=1446,fd=18))
udp    UNCONN  0       0                       [::]:5355             [::]:*      users:(("systemd-resolve",pid=1446,fd=14))
tcp    LISTEN  0       128                  0.0.0.0:5355          0.0.0.0:*      users:(("systemd-resolve",pid=1446,fd=13))
tcp    LISTEN  0       128            127.0.0.53%lo:53            0.0.0.0:*      users:(("systemd-resolve",pid=1446,fd=19))
tcp    LISTEN  0       128                     [::]:5355             [::]:*      users:(("systemd-resolve",pid=1446,fd=15))
```

**LLMNR** est activé par défaut [RFC 4795](https://tools.ietf.org/html/rfc4795)

```bash
[root@fedora31-1 ~]# grep LLMNR /etc/systemd/resolved.conf
LLMNR=no
[root@fedora31-1 ~]# ss -utpanl | grep resolve
udp    UNCONN  0       0              127.0.0.53%lo:53            0.0.0.0:*      users:(("systemd-resolve",pid=4680,fd=13))
tcp    LISTEN  0       128            127.0.0.53%lo:53            0.0.0.0:*      users:(("systemd-resolve",pid=4680,fd=14))
```


**Résolution** via `systemd-resolved`

* Avec `dig`

```bash
[root@fedora31-1 tonychg]# dig @127.0.0.53 +tcp +short ynov.com
217.70.184.38
```

* Avec `systemd-resolve`

```bash
[root@fedora31-1 tonychg]# systemd-resolve ynov.com
ynov.com: 217.70.184.38                        -- link: eth0

-- Information acquired via protocol DNS in 1.6ms.
-- Data is authenticated: no
```

* On **remplace** le fichier `/etc/resolv.conf` par un **lien symbolique** pointant sur la configuration actuelle de `systemd-resolved`

```bash
[root@fedora31-1 tonychg]# mv -v /etc/resolv.conf{,.orig}
renamed '/etc/resolv.conf' -> '/etc/resolv.conf.orig'
[root@fedora31-1 tonychg]# ln -sfv /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
'/etc/resolv.conf' -> '/run/systemd/resolve/stub-resolv.conf'
[root@fedora31-1 tonychg]# grep nameserver /etc/resolv.conf
nameserver 127.0.0.53
[root@fedora31-1 tonychg]# dig +short ynov.com
217.70.184.38
```

On ajoute le serveur `OpenDNS` dans la **configuration** de `systemd-resolved` : `/etc/systemd/resolved.conf`

```bash
[Resolve]
#DNS=
FallbackDNS=1.1.1.1  208.67.222.222 8.8.8.8 1.0.0.1 8.8.4.4 2606:4700:4700::1111 2001:4860:4860::8888 2606:4700:4700::1001 2001:4860:4860::8844
```

On **vérifie** que la configuration est effective

```bash
[root@fedora31-1 tonychg]# resolvectl | grep '208.67.222.222'
                      208.67.222.222
```

* DNS over TLS

**Simultanément** : 

* on réalise une capture sur notre interface principale en filtrant le port 853 ([RFC 7858](https://tools.ietf.org/html/rfc7858))
* on résout le nom `google.com` en utilisant la commande `systemd-resolve`

Le traffic est bien **chiffré**.

```bash
[fedora@fedora31-1 ~]$ sudo tcpdump -i eth0 -n -nn port 853
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
16:25:15.154552 IP 192.168.5.61.60850 > 1.1.1.1.853: Flags [S], seq 2207488229:2207488786, win 64240, options [mss 1460,sackOK,TS val 1331632499 ecr 0,nop,wscale 7,exp-tfo cookie 382810f02e93d7b9], length 557
16:25:15.156174 IP 1.1.1.1.853 > 192.168.5.61.60850: Flags [S.], seq 2648869614, ack 2207488230, win 29200, options [mss 1460,nop,nop,sackOK,nop,wscale 10], length 0
16:25:15.156219 IP 192.168.5.61.60850 > 1.1.1.1.853: Flags [P.], seq 1:558, ack 1, win 502, length 557
16:25:15.157732 IP 1.1.1.1.853 > 192.168.5.61.60850: Flags [.], ack 558, win 30, length 0
16:25:15.157872 IP 1.1.1.1.853 > 192.168.5.61.60850: Flags [P.], seq 1:148, ack 558, win 30, length 147
16:25:15.157896 IP 192.168.5.61.60850 > 1.1.1.1.853: Flags [.], ack 148, win 501, length 0
16:25:15.158101 IP 192.168.5.61.60850 > 1.1.1.1.853: Flags [P.], seq 558:609, ack 148, win 501, length 51
16:25:15.158232 IP 192.168.5.61.60850 > 1.1.1.1.853: Flags [P.], seq 609:640, ack 148, win 501, length 31


[root@fedora31-1 ~]# systemd-resolve google.com
google.com: 172.217.18.206                     -- link: eth0

-- Information acquired via protocol DNS in 3.7ms.
-- Data is authenticated: no

```

### Configuration de systemd-resolved avec OpenVPN

#### OpenVPN avec systemd-resolved

On récupère le script `update-systemd-resolved`

```bash
git clone https://github.com/jonathanio/update-systemd-resolved.git
cd update-systemd-resolved
sudo make
```

#### Désactivation NetworkManager

* `/etc/NetworkManager/NetworkManager.conf`

```
[main]
#plugins=ifcfg-rh,ibft
dns=none
```

#### Configuration de `sytemd-resolved`


* Activation et démarrage `systemd-resolved`

```bash
systemctl restart NetworkManager
systemctl enable --now systemd-resolved
```

* On utilise `stub-resolv.conf` chargé par `systemd`

```bash
ln -sfv /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf

ls -la /etc/resolv.conf
lrwxrwxrwx. 1 root root 37 Dec  8 11:46 /etc/resolv.conf -> /run/systemd/resolve/stub-resolv.conf
```

* On vérifie le `resolv.conf`

```bash
cat /etc/resolv.conf
# This file is managed by man:systemd-resolved(8). Do not edit.
#
# This is a dynamic resolv.conf file for connecting local clients to the
# internal DNS stub resolver of systemd-resolved. This file lists all
# configured search domains.
#
# Run "resolvectl status" to see details about the uplink DNS servers
# currently in use.
#
# Third party programs must not access this file directly, but only through the
# symlink at /etc/resolv.conf. To manage man:resolv.conf(5) in a different way,
# replace this symlink by a static file or a different symlink.
#
# See man:systemd-resolved.service(8) for details about the supported modes of
# operation for /etc/resolv.conf.

nameserver 127.0.0.53
options edns0
search home lab.local
```

#### Configuration du client OpenVPN


On édite la configuration OpenVPN

* client.ovpn`

```
client

tls-client
auth SHA256
cipher AES-256-CBC
remote-cert-tls server
tls-version-min 1.2

# Add this line
script-security 2
setenv PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
up /etc/openvpn/scripts/update-systemd-resolved
up-restart
down /etc/openvpn/scripts/update-systemd-resolved
down-pre

...
```

* Connection au VPN

```bash
sudo openvpn client.ovpn
```

#### Test de la configuration

On recharge le cache

```bash
sudo resolvectl flush-caches
sudo resolvectl reset-server-features
```

On résout le domaine local et internet

```bash
dig ipa.lab.local

; <<>> DiG 9.11.13-RedHat-9.11.13-2.fc31 <<>> ipa.lab.local
;; global options: +cmd
;; Got answer:
;; WARNING: .local is reserved for Multicast DNS
;; You are currently testing what happens when an mDNS query is leaked to DNS
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 48291
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;ipa.lab.local.         IN  A

;; ANSWER SECTION:
ipa.lab.local.      1183    IN  A   192.168.5.10
ipa.lab.local.      1183    IN  A   192.168.7.2

;; Query time: 0 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: Sun Dec 08 12:14:09 CET 2019
;; MSG SIZE  rcvd: 74

dig google.com

; <<>> DiG 9.11.13-RedHat-9.11.13-2.fc31 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 9677
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		171	IN	A	216.58.201.238

;; Query time: 24 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: Sun Dec 08 12:14:40 CET 2019
;; MSG SIZE  rcvd: 55
```

# II. Boot et Logs

* Génération du **graphe**

```bash
ssh 192.168.5.61 sudo systemd-analyze plot > graphe.svg
inkview graphe.svg
```

![graphe](./boot.svg)

* L'**Unité** du processus `chronyd`

```bash
[root@fedora31-1 tonychg]# ps -e -o cmd,unit | grep chronyd
/usr/sbin/chronyd           chronyd.service
```

* Temps de **démarrage** de `sshd.service`

```bash
[root@fedora31-1 ~]# systemd-analyze blame | grep 'sshd'
  46ms sshd.service
```

# III. Mécanismes manipulés par systemd


## 1. cgroups

* cgroups utilisés par ma session SSH

```bash
[fedora@fedora31-1 ~]$ ps -ef -o pid,cmd,cgroup| grep sshd
    872  \_ grep --color=auto sshd  0::/user.slice/user-1000.slice/session-1.scope
[fedora@fedora31-1 ~]$ ls /sys/fs/cgroup/user.slice/user-1000.slice/session-1.scope
cgroup.controllers  cgroup.max.descendants  cgroup.threads  io.pressure          memory.high  memory.oom.group     memory.swap.events  pids.max
cgroup.events       cgroup.procs            cgroup.type     memory.current       memory.low   memory.pressure      memory.swap.max
cgroup.freeze       cgroup.stat             cpu.pressure    memory.events        memory.max   memory.stat          pids.current
cgroup.max.depth    cgroup.subtree_control  cpu.stat        memory.events.local  memory.min   memory.swap.current  pids.events
```

* Le fichier MemoryMax.conf est bien crée

```bash
[root@fedora31-1 ~]# ps -e -o pid,cmd,cgroup | grep sshd
    648 /usr/sbin/sshd -D -oCiphers 0::/system.slice/sshd.service
   1398 sshd: tonychg [priv]        0::/user.slice/user-1002.slice/session-13.scope
   1401 sshd: tonychg@pts/2         0::/user.slice/user-1002.slice/session-13.scope
   2057 sshd: tonychg [priv]        0::/user.slice/user-1002.slice/session-14.scope
   2060 sshd: tonychg@pts/0         0::/user.slice/user-1002.slice/session-14.scope
```

* **Mémoire max** pour ma session SSH

```bash
[root@fedora31-1 ~]# systemctl show user-1002.slice |grep MemoryMax
MemoryMax=infinity
```

* On **limite** la mémoire à 512 mégaoctets

```bash
[root@fedora31-1 ~]# systemctl set-property user-1002.slice MemoryMax=512M
[root@fedora31-1 ~]# systemctl show user-1002.slice |grep MemoryMax
MemoryMax=536870912
DropInPaths=/usr/lib/systemd/system/user-.slice.d/10-defaults.conf /etc/systemd/system.control/user-1002.slice.d/50-MemoryMax.conf
```

```bash
[root@fedora31-1 ~]$ cat /etc/systemd/system.control/user-1002.slice.d/50-MemoryMax.conf 
# This is a drop-in unit file extension, created via "systemctl set-property"
# or an equivalent operation. Do not edit.
[Slice]
MemoryMax=536870912
```


## 2. dbus

* on **capture** le traffic des dbus systèmes avec la commande `dbus-monitor` ou `busctl`

```bash
dbus-monitor --system --pcap > capture.cap
# On appuie sur le changement de luminosité du clavier
Ctrl+Z
```

```bash
busctl capture org.freedesktop.UPower > capture.cap
```

On peut ouvrir la capture dans `wireshark`
```
wireshark capture.cap
```

On observe que le changement de luminosité du clavier est assez simple
Il fait appelle [KbdBacklight](https://upower.freedesktop.org/docs/KbdBacklight.html) sur un `Lenovo Thinkpad T460p`

En envoyant le "field" `BrightnessChanged` la valeur de la lunminosité **augmente** de 1.

```
Frame 7: 172 bytes on wire (1376 bits), 172 bytes captured (1376 bits)
    Encapsulation type: D-Bus (146)
    Arrival Time: Nov 29, 2019 15:31:43.292181000 CET
    [Time shift for this packet: 0.000000000 seconds]
    Epoch Time: 1575037903.292181000 seconds
    [Time delta from previous captured frame: 0.178145000 seconds]
    [Time delta from previous displayed frame: 0.178145000 seconds]
    [Time since reference or first frame: 1.760537000 seconds]
    Frame Number: 7
    Frame Length: 172 bytes (1376 bits)
    Capture Length: 172 bytes (1376 bits)
    [Frame is marked: False]
    [Frame is ignored: False]
    [Protocols in frame: dbus]
D-Bus
    Header
        Endianness Flag: l
        Message Type: Signal emission (4)
        Message Flags: 0x01
        Protocol Version: 1
        Message body Length: 4
        Message Serial (cookie): 1122
        Header fields Length: 151
    Header Field: PATH
        Field code: PATH (1)
        Type signature: o
        OBJECT_PATH: /org/freedesktop/UPower/KbdBacklight
    Header Field: INTERFACE
        Field code: INTERFACE (2)
        Type signature: s
        STRING: org.freedesktop.UPower.KbdBacklight
    Header Field: SIGNATURE
        Field code: SIGNATURE (8)
        Type signature: g
        SIGNATURE: i
    Header Field: MEMBER
        Field code: MEMBER (3)
        Type signature: s
        STRING: BrightnessChanged
    Header Field: SENDER
        Field code: SENDER (7)
        Type signature: s
        STRING: :1.139
    Body
        INT32: 1
```

En **rappuyant** sur le changement de lumiosité, la valeur passe à 2, puis 0

```
Frame 9: 172 bytes on wire (1376 bits), 172 bytes captured (1376 bits)
D-Bus
    Header
        Endianness Flag: l
        Message Type: Signal emission (4)
        Message Flags: 0x01
        Protocol Version: 1
        Message body Length: 4
        Message Serial (cookie): 1124
        Header fields Length: 151
    Header Field: PATH
        Field code: PATH (1)
        Type signature: o
        OBJECT_PATH: /org/freedesktop/UPower/KbdBacklight
    Header Field: INTERFACE
        Field code: INTERFACE (2)
        Type signature: s
        STRING: org.freedesktop.UPower.KbdBacklight
    Header Field: SIGNATURE
        Field code: SIGNATURE (8)
        Type signature: g
        SIGNATURE: i
    Header Field: MEMBER
        Field code: MEMBER (3)
        Type signature: s
        STRING: BrightnessChanged
    Header Field: SENDER
        Field code: SENDER (7)
        Type signature: s
        STRING: :1.139
    Body
        INT32: 2
```

Envoie des évènements en Python avec le module `python-dbus`

```python
# coding: utf-8
"""
    MIT License

    Copyright (c) 2019 oss

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    Un petit script pour faire clignoter le rétro-éclairage
    Usage: sudo python3 blink.py -i <intervale en seconde> -c <nombre d'itération>

    Antoine CHINY - github.com/tonychg
"""

import argparse
import time
import dbus


def kb_light_set(value):
    bus = dbus.SystemBus()
    kbd_backlight_proxy = bus.get_object(
	'org.freedesktop.UPower',
	'/org/freedesktop/UPower/KbdBacklight',
    )
    kbd_backlight = dbus.Interface(kbd_backlight_proxy, 'org.freedesktop.UPower.KbdBacklight')
    kbd_backlight.SetBrightness(value)


def main():
    states = [0, 2]
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--count', type=int, default=10)
    parser.add_argument('-i', '--interval', type=float, default=0.1)
    args = parser.parse_args()

    for i in range(int(args.count)):
        for state in states:
            kb_light_set(state)
            time.sleep(args.interval)
            print('Blinks {}/{}'.format(i, args.count), end='\r')
    print("Finished")


if __name__ == '__main__':
    main()
```

#### Lancement du script

```bash
pip search dbus

sudo python3 blink.py -h
usage: blink.py [-h] [-c COUNT] [-i INTERVAL]

optional arguments:
  -h, --help            show this help message and exit
  -c COUNT, --count COUNT
  -i INTERVAL, --interval INTERVAL
```
## 3. Restriction et isolation

### ajouter un traçage réseau

[![asciicast](https://asciinema.org/a/aPrwkbqlY1BHqDoYb3fverna4.svg)](https://asciinema.org/a/aPrwkbqlY1BHqDoYb3fverna4)

### ajouter des restrictions réseau 

[![asciicast](https://asciinema.org/a/2AnIWLHX6SDKIjKrREud8IrYr.svg)](https://asciinema.org/a/2AnIWLHX6SDKIjKrREud8IrYr)

### `sudo systemd-nspawn --ephemeral --private-network -D / bash`

* `--ephemeral` : On souhaite snapshoter l'arborecense
* `--private-network` : Désactive l'accès réseau (Seul la `loopback` est accessible)

```bash
[root@fedora31-1 ~]# systemd-nspawn --ephemeral --private-network -D / bash
Spawning container fedora31-1-967a239d582b90f7 on /.#machine.ecc08e997ecbb3ff.
Press ^] three times within 1s to kill container.
[root@fedora31-1-967a239d582b90f7 /]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
```

### différents namespaces

```bash
[root@fedora31-1 ~]# ls -la /proc/22510/ns
total 0
dr-x--x--x. 2 root root 0 Dec  5 09:24 .
dr-xr-xr-x. 9 root root 0 Dec  5 09:24 ..
lrwxrwxrwx. 1 root root 0 Dec  5 09:24 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:31 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:31 mnt -> 'mnt:[4026531840]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:31 net -> 'net:[4026531992]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:31 pid -> 'pid:[4026531836]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:31 pid_for_children -> 'pid:[4026531836]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:31 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:31 uts -> 'uts:[4026531838]'
```

```bash
[root@fedora31-1-967a239d582b90f7 /]# ls -lat /proc/1/ns/
total 0
lrwxrwxrwx. 1 root root 0 Dec  5 09:34 cgroup -> 'cgroup:[4026532280]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:34 ipc -> 'ipc:[4026532223]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:34 mnt -> 'mnt:[4026532221]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:34 net -> 'net:[4026532225]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:34 pid -> 'pid:[4026532224]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:34 pid_for_children -> 'pid:[4026532224]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:34 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 root root 0 Dec  5 09:34 uts -> 'uts:[4026532222]'
```

### ajout d'une restriction

```bash
[root@fedora31-1 /]# systemd-nspawn --drop-capability=CAP_CHOWN --ephemeral --private-network -D / bash
Spawning container fedora31-1-a0eebaac3648adfe on /.#machine.26ff956b70f8e9a7.
Press ^] three times within 1s to kill container.
[root@fedora31-1-a0eebaac3648adfe /]# touch test && chown fedora: test
chown: changing ownership of 'test': Operation not permitted
```

# IV. systemd units in-depth

## 1. Exploration de services existants

### `auditd`

```bash
[root@fedora31-3 ~]# systemctl status auditd
● auditd.service - Security Auditing Service
   Loaded: loaded (/usr/lib/systemd/system/auditd.service; enabled; vendor preset: enabled)
   Active: active (running) since Sun 2019-12-01 13:14:56 UTC; 3 days ago
     Docs: man:auditd(8)
           https://github.com/linux-audit/audit-documentation
  Process: 509 ExecStart=/sbin/auditd (code=exited, status=0/SUCCESS)
  Process: 517 ExecStartPost=/sbin/augenrules --load (code=exited, status=0/SUCCESS)
 Main PID: 512 (auditd)
    Tasks: 2 (limit: 4685)
   Memory: 7.5M
      CPU: 826ms
   CGroup: /system.slice/auditd.service
           └─512 /sbin/auditd

Dec 01 13:14:56 fedora31-3 systemd[1]: Starting Security Auditing Service...
Dec 01 13:14:56 fedora31-3 auditd[512]: No plugins found, not dispatching events
Dec 01 13:14:56 fedora31-3 auditd[512]: Init complete, auditd 3.0 listening for events (startup state enable)
Dec 01 13:14:56 fedora31-3 augenrules[517]: /sbin/augenrules: No change
Dec 01 13:14:56 fedora31-3 augenrules[517]: No rules
Dec 01 13:14:56 fedora31-3 systemd[1]: Started Security Auditing Service.
```

* Le chemin : `/usr/lib/systemd/system/auditd.service`
* `ExecStartPost=/sbin/augenrules --load` : une fois le démon `auditd` lancé, il recharge les règles de controles
* On peut ajouter une règle non persistante de controle avec la commande `auditctl -a <l,a>`
* Ou directement ajouter la règle dans `/etc/audit/rules.d/audit.rules`

```
# Pour **surveiller** les modifications dans le répertoire /etc
-w /etc/ -p wa -k audit_conf
```

Les configurations de sécurité :

```bash
# Impossible de charger de nouveau module kernel
ProtectKernelModules=true
# cgroups en Read-only
ProtectControlGroups=true
# Rend impossible le changement du "memory mapping"
MemoryDenyWriteExecute=true
LockPersonality=true
```

## 2. Création de service simple

```go
// Created by tonychg at 12/05/19  
// Governed by a license that can be found in the LICENSE file.

package main

import (
	"os"
	"net/http"
	"log"

	"github.com/labstack/echo"
)

// User
type ResponseData struct {
	Message  string `json:"message" xml:"message"`
}

// Handler
func ping(c echo.Context) error {
	m := &ResponseData{
		Message:  "PONG",
	}
	log.Println("PONG respond to PING")
	return c.JSON(http.StatusOK, m)
}

func main() {
	bindAddress := "127.0.0.1"
	bindPort := "1323"
	// Disable log timestamps beacause we use systemd units
	log.SetFlags(log.Flags() &^ (log.Ldate | log.Ltime))

	if os.Getenv("BIND_ADDRESS") != "" {
		bindAddress = os.Getenv("BIND_ADDRESS")
	}

	if os.Getenv("BIND_PORT") != "" {
		bindPort = os.Getenv("BIND_PORT")
	}

	e := echo.New()
	e.GET("/ping", ping)
	e.Logger.Fatal(e.Start(bindAddress + ":" + bindPort))
}
```

#### Configurations `systemd`

Pour pouvoir à la fois modifier les **règles de filtrage** de `firewalld`.

Tout en évitant de lancer le serveur web en `root` 🙂

Nous allons utiliser **deux** unités **différentes** :
* une qui tournera en `root` pour modifier les règles
* une qui tournera avec l'utilisateur `webserver-user`

Pour que les deux unités se lancent en même temps.

On spécifie dans l'unité `webserver-firewalld.service` l'attribut `PartOf=webserver.service`

https://www.freedesktop.org/software/systemd/man/systemd.unit.html

* `/etc/systemd/system/webserver-firewalld.service`

```
[Unit]
Description=Open ports required by webserver.service
After=network-online.target
PartOf=webserver.service

[Service]
Type=oneshot

RemainAfterExit=yes

EnvironmentFile=/etc/default/webserver

ExecStart=firewall-cmd --add-port=${BIND_PORT}/tcp --permanent --zone=public
ExecStartPost=firewall-cmd --reload

ExecStop=firewall-cmd --remove-port=${BIND_PORT}/tcp --permanent --zone=public
ExecStopPost=firewall-cmd --reload

[Install]
WantedBy=multi-user.target
```

* `/etc/systemd/system/webserver.service`

```
[Unit]
Description=Very basic Golang http server using echo framework
Documentation=https://github.com/labstack/echo
After=webserver-firewalld.service
Wants=webserver-firewalld.service
AssertFileIsExecutable=/usr/local/bin/webserver

[Service]
WorkingDirectory=/usr/local/

EnvironmentFile=/etc/default/webserver

User=webserver-user
Group=webserver-user

ExecStart=/usr/local/bin/webserver

# Let systemd restart this service always
Restart=always

# Specifies the maximum file descriptor number that can be opened by this process
LimitNOFILE=65536
MaxMemory=512M

# Disable timeout logic and wait until process is stopped
TimeoutStopSec=infinity
SendSIGKILL=no

[Install]
WantedBy=multi-user.target
```

* `/etc/default/webserver`

```bash
BIND_ADDRESS=0.0.0.0
BIND_PORT=5000
```

* On active les services

```bash
sudo systemctl enable firewalld
sudo systemctl enable webserver
```

### On vérifie que tout fonctionne encore 🙂

[![asciicast](https://asciinema.org/a/fsqLq9vpDf4kDxIKXdQTVz03S.svg)](https://asciinema.org/a/fsqLq9vpDf4kDxIKXdQTVz03S)

* `systemctl enable <unit>`

`systemctl enable <unit>` crée un lien symbolique dans la `multi-user.target`. Cela a pour conséquence de lancer le service en même temps que tout les services du run-level: multi-user.

```bash
[root@fedora31-1 ~]# systemd-analyze security  | grep webserver.service
webserver.service                         9.1 UNSAFE    😨
```

## 3. Sandboxing (heavy security)

### On ajoute ces configurations

```bash
# Security
ProtectHome=read-only				    # namespace: readonly filesystem
ProtectHostname=yes				    # namespace: prevent hostname change
ProtectKernelModules=yes			    # prevent kernel module loading
ProtectKernelTunables=yes			    # /proc/sys, /sys, .... /proc/irq will be made read-only
ProtectSystem=strict				    # readonly /etc /usr /boot
NoNewPrivileges=yes
LockPersonality=yes
RestrictSUIDSGID=yes
MemoryMax=512M					    # cgroups: Limit the memory

CapabilityBoundingSet=-CAP_SET
RestrictAddressFamilies=AF_UNIX AF_INET AF_INET6    # Prevent exotic open socket

PrivateDevices=yes				    # Private namespace for devices /dev/null /dev/zero, disable CAP_MKNOD
PrivateTmp=yes					    # Namespace filesystem /tmp/systemd-private-webserver/tmp
PrivateUsers=yes

IPAddressDeny=any
IPAddressAllow=192.168.5.0/24
```

```bash
[root@fedora31-1 ~]# systemd-analyze security  | grep webserver.service
webserver.service                         4.0 OK        🙂
```

### On vérifie que tout fonctionne encore 🙂

[![asciicast](https://asciinema.org/a/kDPhMMlxeMgR4eepb7TFCmuFd.svg)](https://asciinema.org/a/kDPhMMlxeMgR4eepb7TFCmuFd)
