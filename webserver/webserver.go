// Created by tonychg at 12/05/19  
// Governed by a license that can be found in the LICENSE file.

package main

import (
	"os"
	"net/http"
	"log"

	"github.com/labstack/echo"
)

// User
type ResponseData struct {
	Message  string `json:"message" xml:"message"`
}

// Handler
func ping(c echo.Context) error {
	m := &ResponseData{
		Message:  "PONG",
	}
	log.Println("PONG respond to PING")
	return c.JSON(http.StatusOK, m)
}

func main() {
	bindAddress := "127.0.0.1"
	bindPort := "1323"
	log.SetFlags(log.Flags() &^ (log.Ldate | log.Ltime))

	if os.Getenv("BIND_ADDRESS") != "" {
		bindAddress = os.Getenv("BIND_ADDRESS")
	}

	if os.Getenv("BIND_PORT") != "" {
		bindPort = os.Getenv("BIND_PORT")
	}

	e := echo.New()
	e.GET("/ping", ping)
	e.Logger.Fatal(e.Start(bindAddress + ":" + bindPort))
}
