Easy build with `podman`
------------------------

You need `podman` and `make`

On Fedora 31

```bash
dnf install -y podman make

make
```

Using `go` binary
-----------------

You can also install `go` binaries and the micro framework [echo](https://github.com/labstack/echo)

```bash
dnf install -y golang
go get -u 'github.com/labstack/echo/...'
```

And building

```bash
go build -v -o webserver-linux-amd64
```

Run the HTTP web server
-----------------------

```bash
BIND_ADDRESS=127.0.0.1 BIND_PORT=5000 ./webserver-linux-amd64
```

Test the web service
--------------------

```bash
curl -s http://127.0.0.1:5000/
{"message":"Not Found"}
curl -s http://127.0.0.1:5000/ping
{"message":"PONG"}
```

The full pipeline
--------------

[![asciicast](https://asciinema.org/a/TA5XTLVS8S0x2PxesmqvbSQNa.svg)](https://asciinema.org/a/TA5XTLVS8S0x2PxesmqvbSQNa)
