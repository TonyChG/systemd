# coding: utf-8
"""
    MIT License

    Copyright (c) 2019 oss

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

    Un petit script pour faire clignoter le rétro-éclairage
    Usage: sudo python3 blink.py -i <intervale en seconde> -c <nombre d'itération>

    Antoine CHINY - github.com/tonychg
"""

import argparse
import time
import dbus


def kb_light_set(value):
    bus = dbus.SystemBus()
    kbd_backlight_proxy = bus.get_object('org.freedesktop.UPower', '/org/freedesktop/UPower/KbdBacklight')
    kbd_backlight = dbus.Interface(kbd_backlight_proxy, 'org.freedesktop.UPower.KbdBacklight')
    kbd_backlight.SetBrightness(value)


def main():
    states = [0, 2]
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--count', type=int, default=10)
    parser.add_argument('-i', '--interval', type=float, default=0.1)
    args = parser.parse_args()

    for i in range(int(args.count)):
        for state in states:
            kb_light_set(state)
            time.sleep(args.interval)
            print('Blinks {}/{}'.format(i, args.count), end='\r')
    print("Finished")


if __name__ == '__main__':
    main()
